
void setup() {
  // put your setup code here, to run once:
  pinMode(2, INPUT);    //initializarea iesirilor/intrarilor Arduino
  pinMode(3, INPUT);

  pinMode(8, OUTPUT);
  pinMode(7, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:

  int senzorInput = digitalRead(2);
  int senzorInput2 = digitalRead(3);

  if(senzorInput == HIGH) //cand senzorul nu detecteaza magnetul
    digitalWrite(7, LOW); //stinge mosfet
  else                    //cand senzorul detecteaza magnet
    digitalWrite(7, HIGH);    //aprinde mosfet

  if(senzorInput2 == HIGH)
    digitalWrite(8, LOW);
  else
    digitalWrite(8, HIGH);

  delay(10);
}
